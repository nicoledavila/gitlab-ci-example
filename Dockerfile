FROM node:8.1.2-alpine

EXPOSE 3000

WORKDIR /app

COPY . /app

RUN npm install

CMD npm start
