# Gitlab CI Example

This is an example project that shows how to use Gitlab CI do deploy an application to Heroku.

## How to run

```
npm start
```

Point your browser to `http://localhost:3000`. This should work...otherwise, nobody's fault by mine!

## How to test

```
npm test
```

## How to deploy

1. Fork this repository
2. Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)
3. Create a Heroku app with `heroku app:create <app-name>`
4. Run `heroku auth:token` to get the `<auth-token>`
5. Go to Gitlab's Application Pipeline settings (https://gitlab.com/<your-user>/gitlab-ci-example/settings/ci_cd) and add the following Secret variables:

```
APP_NAME = <app-name>>
AUTH_TOKEN = <auth-token>
```

6. Go to the Pipelines view in Gitlab (https://gitlab.com/<your-user>/gitlab-ci-example/pipelines) and click on `Run Pipeline`
