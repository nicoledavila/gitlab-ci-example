import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';

console.log("loading application...");

ReactDOM.render(<App />, document.getElementById('app'));